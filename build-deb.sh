#!/bin/bash
# Build a biary .deb from files installed to deb/
PKG=atom-wxwidgets
VERS=`grep "Version:\s.*" deb/DEBIAN/control | grep -o "[[:digit:]].*"`
PVER=${PKG}_${VERS}
echo "building $PVER"
cp -a deb $PVER
# remove .gitignore from the deb contents
rm $PVER/usr/local/.gitignore
dpkg-deb --build $PVER
rm -rf $PVER

